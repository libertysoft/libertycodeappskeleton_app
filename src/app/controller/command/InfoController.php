<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace app\app\controller\command;

use liberty_code\controller\controller\model\DefaultController;

use liberty_code\request_flow\response\model\DefaultResponse;



class InfoController extends DefaultController
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function checkAccessMethodEngine($strMethodNm, array $tabArg = null)
    {
        // Return result
        return true;
    }





    // Methods action
    // ******************************************************************************

    /**
     * Action to get info.
     *
     * @return DefaultResponse
     */
    public function actionGet()
    {
        // Get response
        $objResponse = new DefaultResponse();
        $objResponse->setContent('Hello word' . PHP_EOL);

        // Return response
        return $objResponse;
    }



}