<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace app\app\controller\web;

use liberty_code\controller\controller\model\DefaultController;

use liberty_code\view\view\factory\api\ViewFactoryInterface;
use liberty_code\http\request_flow\response\model\HttpResponse;
use liberty_code\framework\request_flow\request\http\model\HttpRequest;



class IndexController extends DefaultController
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: HTTP request instance.
     * @var HttpRequest
     */
    protected $objRequest;



    /**
     * DI: View factory instance.
     * @var ViewFactoryInterface
     */
    protected $objViewFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param HttpRequest $objRequest
     * @param ViewFactoryInterface $objViewFactory
     */
    public function __construct(
        HttpRequest $objRequest,
        ViewFactoryInterface $objViewFactory
    )
    {
        // Init properties
        $this->objRequest = $objRequest;
        $this->objViewFactory = $objViewFactory;

        // Call parent constructor
        parent::__construct();
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function checkAccessMethodEngine($strMethodNm, array $tabArg = null)
    {
        // Return result
        return true;
    }





    // Methods action
    // ******************************************************************************

    /**
     * Action to get index page.
     *
     * @return HttpResponse
     */
    public function actionGet()
    {
        // Get render
        $strRender = $this
            ->objViewFactory
            ->getObjView(array(
                'key' => 'app.index.index'
            ))
            ->getStrRender();

        // Get response
        $objResponse = new HttpResponse();
        $objResponse->setIntStatusCode(200);
        $objResponse->setContent($strRender);

        // Return response
        return $objResponse;
    }



}