<?php

namespace app\app\migration;

use liberty_code_module\migration\migration\command\fix\model\FixCommandMigration;

use liberty_code\migration\migration\library\ConstMigration;
use liberty_code\migration\migration\version\library\ConstVersionMigration;

class Migration2 extends FixCommandMigration
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * Get fixed configuration array.
     *
     * @return array
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            ConstMigration::TAB_CONFIG_KEY_KEY => 'Migration2',
            ConstVersionMigration::TAB_CONFIG_KEY_VERSION => '1.0.0',
            ConstVersionMigration::TAB_CONFIG_KEY_ORDER => 1,
            ConstVersionMigration::TAB_CONFIG_KEY_SORT_COMPARE_USE_KEY => true
        );
    }





    // Methods execute
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function executeMigration()
    {
        echo('Execute ' . $this->getStrKey() . PHP_EOL);
    }



    /**
     * @inheritdoc
     */
    protected function rollbackMigration()
    {
        echo('Rollback ' . $this->getStrKey() . PHP_EOL);
    }



}