<?php

namespace app\app\boot;

use liberty_code\framework\bootstrap\model\DefaultBootstrap;

use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\framework\library\path\library\ToolBoxPath;
use liberty_code\framework\framework\library\ConstFramework;



class AppBootstrap extends DefaultBootstrap
{
    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct(AppInterface $objApp)
    {
        // Call parent constructor
        parent::__construct($objApp, 'app');
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Boot module.
     */
    public function boot()
    {
        // Create directories, if required
        $tabDirPath = array(
            ConstFramework::CONF_PATH_DIR_VAR . '/app',
            ConstFramework::CONF_PATH_DIR_VAR . '/app/cache'
        );

        foreach($tabDirPath as $strDirPath)
        {
            // Get directory full path
            $strDirPath = ToolBoxPath::getStrPathFull($strDirPath);

            // Create directory, if required
            if(!is_dir($strDirPath))
            {
                mkdir($strDirPath);
            }
        }
    }



}