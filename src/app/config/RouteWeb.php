<?php

use app\app\controller\web\IndexController;



return array(
    // Index routes
    // ******************************************************************************

    'index_get' => [
        'source' => '/',
        'source_method' => 'get',
        'call' => [
            'class_path_pattern' => IndexController::class . ':actionGet'
        ]
    ]
);