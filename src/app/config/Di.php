<?php

use liberty_code\register\register\table\model\DefaultTableRegister;
use liberty_code\cache\format\model\FormatData;
use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\cache\repository\format\model\FormatRepository;
use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\file\register\directory\model\DirRegister;
use liberty_code\model\persistence\api\PersistorInterface;
use liberty_code\item_browser\operation\model\OperatorData;
use liberty_code\item_browser\browser\api\BrowserInterface;
use liberty_code\sql\database\connection\api\ConnectionInterface;
use liberty_code\sql\database\command\factory\api\CommandFactoryInterface;
use liberty_code\sql\database\command\factory\standard\model\StandardCommandFactory;
use liberty_code\sql\persistence\table\model\TablePersistor;
use liberty_code\sql\persistence\data\model\DataPersistor;
use liberty_code\sql\browser\table\model\TableBrowser;
use liberty_code\sql\browser\data\model\DataBrowser;
use liberty_code\mysql\database\connection\pdo\model\MysqlPdoConnection;



return array(
    // SQL database services
    // ******************************************************************************

    'sql_connection' => [
        'source' => MysqlPdoConnection::class,
        'argument' => [
            ['type' => 'array', 'value' => [
                ['key' => 'host', 'type' => 'config', 'value' => 'database/mysql/host'],
                ['key' => 'port', 'type' => 'config', 'value' => 'database/mysql/port'],
                ['key' => 'db_name', 'type' => 'config', 'value' => 'database/mysql/db_name'],
                ['key' => 'charset', 'type' => 'config', 'value' => 'database/mysql/charset'],
                ['key' => 'login', 'type' => 'config', 'value' => 'database/mysql/login'],
                ['key' => 'password', 'type' => 'config', 'value' => 'database/mysql/password']
            ]]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'sql_command_factory' => [
        'source' => StandardCommandFactory::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'sql_connection'],
            ['type' => 'class', 'value' => ProviderInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'sql_table_persistor' => [
        'source' => TablePersistor::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'sql_command_factory']
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'sql_data_persistor' => [
        'source' => DataPersistor::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'sql_command_factory']
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'sql_table_browser' => [
        'source' => TableBrowser::class,
        'argument' => [
            ['type' => 'class', 'value' => OperatorData::class],
            ['type' => 'dependency', 'value' => 'sql_command_factory'],
            ['type' => 'class', 'value' => DefaultTableRegister::class]
        ]
    ],

    'sql_data_browser' => [
        'source' => DataBrowser::class,
        'argument' => [
            ['type' => 'class', 'value' => OperatorData::class],
            ['type' => 'dependency', 'value' => 'sql_command_factory'],
            ['type' => 'class', 'value' => DefaultTableRegister::class]
        ]
    ],



    // Cache services
    // ******************************************************************************

    'cache_register' => [
        'source' => DirRegister::class,
        'argument' => [
            ['type' => 'array', 'value' => [
                ['key' => 'store_dir_path', 'type' => 'config', 'value' => 'cache/file/store_dir_path'],
                ['key' => 'set_timezone_name', 'type' => 'config', 'value' => 'cache/file/set_timezone_name']
            ]]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'cache_repository' => [
        'source' => FormatRepository::class,
        'argument' => [
            ['type' => 'mixed', 'value' => null],
            ['type' => 'dependency', 'value' => 'cache_register'],
            ['type' => 'class', 'value' => FormatData::class],
            ['type' => 'class', 'value' => FormatData::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // Preferences
    // ******************************************************************************

    ConnectionInterface::class => [
        'set' => ['type' => 'dependency', 'value' => 'sql_connection'],
        'option' => [
            'shared' => true
        ]
    ],

    CommandFactoryInterface::class => [
        'set' => ['type' => 'dependency', 'value' => 'sql_command_factory'],
        'option' => [
            'shared' => true
        ]
    ],

    PersistorInterface::class => [
        'set' => ['type' => 'dependency', 'value' => 'sql_table_persistor'],
        'option' => [
            'shared' => true
        ]
    ],

    BrowserInterface::class => [
        'set' => ['type' => 'dependency', 'value' => 'sql_table_browser']
    ],

    RepositoryInterface::class => [
        'set' => ['type' => 'dependency', 'value' => 'cache_repository']
    ]
);