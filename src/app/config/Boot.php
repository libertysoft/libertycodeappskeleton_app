<?php

use app\app\boot\AppBootstrap;



return array(
    'app_bootstrap' => [
        'call' => [
            'class_path_pattern' => AppBootstrap::class . ':boot'
        ]
    ]
);