<?php

use app\app\controller\command\InfoController;



return array(
    // Info routes
    // ******************************************************************************

    'info_get' => [
        'source' => 'app:info:get',
        'call' => [
            'class_path_pattern' => InfoController::class . ':actionGet'
        ],
        'description' => 'Get application info'
    ]
);