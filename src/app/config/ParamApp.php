<?php

use liberty_code\error\build\model\DefaultBuilder as HandlerBuilder;
use liberty_code\error\build\directory\model\DirBuilder as HandlerDirBuilder;
use liberty_code\error\warning\build\model\DefaultBuilder as WarnHandlerBuilder;
use liberty_code\error\warning\build\directory\model\DirBuilder as WarnHandlerDirBuilder;
use liberty_code\model\datetime\factory\model\DefaultDateTimeFactory;
use liberty_code\migration\build\model\DefaultBuilder as MigBuilder;
use liberty_code\migration\build\directory\model\DirBuilder as MigDirBuilder;



return array(
    // Database configuration
    // ******************************************************************************

    'database' => [
        'mysql' => [
            'host' => 'localhost',
            'port' => '3306',
            'db_name' => 'libertycode',
            'charset' => 'utf8',
            'login' => 'root',
            'password' => ''
        ]
    ],



    // Cache configuration
    // ******************************************************************************

    'cache' => [
        'file' => [
            'store_dir_path' => dirname(__FILE__) . '/../../var/app/cache',
            'set_timezone_name' => 'UTC'
        ]
    ],



    // Datetime configuration
    // ******************************************************************************

    'datetime' => [
        /**
         * Datetime configuration used on client side.
         */
        'client' => [
            /**
             * Datetime configuration used to render dates.
             */
            'get' => [
                'timezone' => 'America/New_York',
                /**
                 * Datetime format can be string|string[].
                 * @see DefaultDateTimeFactory get_datetime_format configuration format
                 * If list provided, first is considered as datetime format base,
                 * @see DefaultDateTimeFactory note about datetime format base.
                 *
                 */
                'format' => [
                    'long' => 'd/m/Y H:i:s',
                    'short' => 'd/m/Y'
                ]
            ],

            /**
             * Datetime configuration used to hydrate dates.
             */
            'set' => [
                'timezone' => 'America/New_York',
                /**
                 * Datetime format can be string|string[].
                 * @see DefaultDateTimeFactory set_datetime_format configuration format
                 * If list provided, first is considered as datetime format base,
                 * @see DefaultDateTimeFactory note about datetime format base.
                 *
                 */
                'format' => [
                    'long' => 'd/m/Y H:i:s',
                    'short' => 'd/m/Y'
                ]
            ]
        ]
    ],



    // Error configuration
    // ******************************************************************************

    'error' => [
        // Application execution configuration
        'execution' => [
            'handler' => [
                /**
                 * Configuration used by default builder,
                 * to populate handler collection,
                 * used during application execution.
                 *
                 * Format: @see HandlerBuilder data source format
                 */
                'config' => [],

                /**
                 * Configuration used by directory builder,
                 * to populate handler collection,
                 * used during application execution.
                 *
                 * Format: @see HandlerDirBuilder data source format
                 */
                'dir_path' => [
                    dirname(__FILE__) . '/../error/execution'
                ]
            ],

            'warning' => [
                'handler' => [
                    /**
                     * Configuration used by default builder,
                     * to populate warning handler collection,
                     * used during application execution.
                     *
                     * Format: @see WarnHandlerBuilder data source format
                     */
                    'config' => [],

                    /**
                     * Configuration used by directory builder,
                     * to populate warning handler collection,
                     * used during application execution.
                     *
                     * Format: @see WarnHandlerDirBuilder data source format
                     */
                    'dir_path' => [
                        dirname(__FILE__) . '/../error/execution/warning'
                    ]
                ]
            ]
        ]
    ],



    // Migration configuration
    // ******************************************************************************

    'migration' => [
        /**
         * Configuration used by default builder,
         * to populate migration collection.
         *
         * Format: @see MigBuilder data source format
         */
        'config' => [],

        /**
         * Configuration used by directory builder,
         * to populate migration collection.
         *
         * Format: @see MigDirBuilder data source format
         */
        'dir_path' => [
            dirname(__FILE__) . '/../migration'
        ]
    ],



    // View configuration
    // ******************************************************************************

    'view' => [
        // Template configuration
        'template' => [
            'repository' => [
                'config' => [
                    'cache_require' => false
                ]
            ]
        ],

        // Compiler configuration
        'compiler' => [
            'php' => [
                'config' => [
                    'cache_require' => false
                ]
            ]
        ],

        // Viewer configuration
        'viewer' => [
            'config' =>[
                'cache_require' => true
            ],
            'add_argument' => [
                'appNm' => 'LibertyCode',
                'appVers' => 'v1.0.0'
            ]
        ]
    ]
);