<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace app\app\error\execution\warning;

use liberty_code\framework\error\warning\handler\throwable\fix\model\FixExecThrowableWarnHandler;

use Throwable;
use ErrorException;
use liberty_code\request_flow\response\api\ResponseInterface;
use liberty_code\request_flow\response\model\DefaultResponse;
use liberty_code\http\request_flow\response\model\HttpResponse;
use liberty_code\error\warning\handler\throwable\library\ConstThrowableWarnHandler;



class ExceptionThrowableWarnHandler extends FixExecThrowableWarnHandler
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            // Index array of exception class paths to include in handling
            ConstThrowableWarnHandler::TAB_CONFIG_KEY_THROW_CLASS_PATH_INCLUDE => null,

            // Index array of exception class paths to exclude from handling
            ConstThrowableWarnHandler::TAB_CONFIG_KEY_THROW_CLASS_PATH_EXCLUDE => [
                ErrorException::class
            ]
        );
    }



    /**
     * @inheritdoc
     * @param Throwable[] $tabWarning
     * @param DefaultResponse $objExecutionResponse
     */
    protected function getObjResponse(
        array $tabWarning,
        ResponseInterface $objExecutionResponse,
        $boolExecutionSuccess
    )
    {
        // Instructions to handle warning exceptions

        // Get render
        $strRender = '';
        foreach($tabWarning as $warning)
        {
            $strRender .=
                ((trim($strRender) != '') ? PHP_EOL : '') .
                sprintf(
                    'Following warning exception occurs: %1$s (code: %2$s) on file \'%3$s\' on line %4$s.',
                    $warning->getMessage(),
                    $warning->getCode(),
                    $warning->getFile(),
                    $warning->getLine()
                );
        }

        // Get response
        $objResponse = $objExecutionResponse;
        $strRespContent = $objResponse->getContent();
        $strRender =
            ((trim($strRespContent) != '') ? PHP_EOL . PHP_EOL : '') .
            $strRender;
        $strRender = (
            ($objResponse instanceof HttpResponse) ?
                str_replace(PHP_EOL, '<br />' . PHP_EOL, $strRender) :
                $strRender
        );
        $objResponse->setContent($strRespContent . $strRender);

        // Return response
        return $objResponse;
    }



}