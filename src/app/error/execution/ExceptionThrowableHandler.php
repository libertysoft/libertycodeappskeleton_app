<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace app\app\error\execution;

use liberty_code\framework\error\handler\throwable\fix\model\FixExecThrowableHandler;

use Throwable;
use Error;
use ErrorException;
use liberty_code\request_flow\request\api\RequestInterface;
use liberty_code\request_flow\response\model\DefaultResponse;
use liberty_code\http\request_flow\request\model\HttpRequest;
use liberty_code\http\request_flow\response\model\HttpResponse;
use liberty_code\error\handler\throwable\library\ConstThrowableHandler;



class ExceptionThrowableHandler extends FixExecThrowableHandler
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * DI: Request instance.
     * @var RequestInterface
     */
    protected $objRequest;


	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param RequestInterface $objRequest
     */
    public function __construct(
        RequestInterface $objRequest
    )
    {
        // Init var
        $this->objRequest = $objRequest;

        // Call parent constructor
        parent::__construct();
    }





    // Methods getters
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function getTabFixConfig()
    {
        // Return result
        return array(
            // Index array of exception class paths to include in handling
            ConstThrowableHandler::TAB_CONFIG_KEY_THROW_CLASS_PATH_INCLUDE => null,

            // Index array of exception class paths to exclude from handling
            ConstThrowableHandler::TAB_CONFIG_KEY_THROW_CLASS_PATH_EXCLUDE => [
                Error::class,
                ErrorException::class
            ]
        );
    }



    /**
     * @inheritdoc
     * @param Throwable $error
     * @return DefaultResponse|HttpResponse
     */
    protected function getObjResponse($error)
    {
        // Instructions to handle exceptions

        // Get render
        $strRender = sprintf(
            'Following exception occurs: %1$s (code: %2$s) on file \'%3$s\' on line %4$s.',
            $error->getMessage(),
            $error->getCode(),
            $error->getFile(),
            $error->getLine()
        );

        // Get response
        $objResponse = new DefaultResponse();
        if($this->objRequest instanceof HttpRequest)
        {
            $objResponse = new HttpResponse();
            $objResponse->setIntStatusCode(500);
        }
        $objResponse->setContent($strRender);

        // Return response
        return $objResponse;
    }



}