LibertyCodeAppSkeleton_App
==========================



Description
-----------

LibertyCode application skeleton application contains main components, 
to create and run basic application.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Installation
------------

1. Requirement
    
    - Git

        It can require git installation, 
        if used for installation.
        For more information: https://git-scm.com/

    - Composer

        It requires composer installation.
        For more information: https://getcomposer.org

2. Command: Installation

    Several ways are possible:
    
    - Composer
    
        ```sh
        php composer.phar create-project "liberty_code_app_skeleton/app" ["<project_root_dir_path>"] ["<version>"]
        ```

    - Download
            
        Several ways are possible:
        
        - Git
    
            ```sh
            git clone https://bitbucket.org/libertysoft/libertycodeappskeleton_app.git
            ```
    
        - Manual download

            Get repository from "https://bitbucket.org/libertysoft/libertycodeappskeleton_app/downloads/"

3. Command: Move in project root directory
    
    ```sh
    cd "<project_root_dir_path>"
    ```

4. Command: External libraries installation
    
    Required only if not installed via composer.
    
    ```sh
    php composer.phar install
    ```
    
5. Note

    - External libraries installation
    
        All external libraries are listed 
        on composer file "<project_root_dir_path>/composer.json".
     
        Instruction are set for composer. 
        It's possible to use any way,
        while all external libraries, 
        listed on composer configuration, 
        are installed and correctly configured,
        on module configuration file "<project_root_dir_path>/config/Module.<config_file_ext>".

---



Application structure
---------------------

#### Configuration [compulsory]

Configuration directory contains all application configuration.
PHP format is used.
It's possible to set other available format (JSON, XML, YML, ...), 
in application bootstrap.

_Elements_

- Application mode file

    Stores application modes available.

- Module file
 
    Registers modules available in application.

#### Source [suggested]

Source directory contains all application source code, 
structured by modules.

#### Resource [suggested]

Resource directory contains all application front resources, 
like views, assets, ....

#### Bin [suggested]

Bin directory contains command line application 
and other command line executables.

_Elements_

- Application file

    Application bootstrap for command line.

#### Web [suggested]

Web directory contains web application 
and other web accessible components.

_Elements_

- Index file

    Application bootstrap for web.

#### Variable [suggested/compulsory, generated]

Variable directory contains all temporary and/or flexible components, 
generated by application.

_Elements_

- Cache directories

    Used by caches for application, framework, view ...
    
- Log directory

    Store logs.

---



Module structure
----------------

#### Module configuration [compulsory]

Module file contains module macro information.

#### Configuration [compulsory]

Configuration directory contains module configuration.
By default the application configuration format is used.
You can set other available format (JSON, XML, PHP, ...), 
in application configuration module file.

_Elements_

- Parameters file ("<module_root_dir_path>/config/Param.<config_file_ext>")
    
    Stores module parameters, 
    available in module.
    
- Application parameters file ("<module_root_dir_path>/config/Param.<config_file_ext>")
    
    Stores application parameters, 
    available in application.
    
- Auto-loading file ("<module_root_dir_path>/config/Autoload.<config_file_ext>")

    Stores module auto-loading rules, 
    available in application.

- Dependency injection file ("<module_root_dir_path>/config/Di.<config_file_ext>")

    Stores module dependencies, 
    available in application.
    
- Register file ("<module_root_dir_path>/config/Register.<config_file_ext>")

    Stores module key-value pairs, 
    available in application.

- Events file ("<module_root_dir_path>/config/Event.<config_file_ext>")

    Stores module event, 
    available in application.

- HTTP routes file ("<module_root_dir_path>/config/RouteWeb.<config_file_ext>")

    Stores module web routes, 
    available in application.
    
- Command line routes file ("<module_root_dir_path>/config/RouteCommand.<config_file_ext>")

    Stores module command line routes, 
    available in application.

- Boot file ("<module_root_dir_path>/config/Boot.<config_file_ext>")

    Stores module bootstrap files, 
    available in application.

---


