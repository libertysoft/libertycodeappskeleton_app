<?php
/**
 * Bootstrap
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */



// Init var
$strRootAppPath = dirname(__FILE__) . '/..';

// Load external library
require_once($strRootAppPath.'/vendor/autoload.php'); // Composer vendor

// Use
use liberty_code\parser\parser\string_table\library\ConstStrTableParser;
use liberty_code\parser\parser\factory\library\ConstParserFactory;
use liberty_code\parser\parser\factory\string_table\library\ConstStrTableParserFactory;
use liberty_code\parser\build\library\ConstBuilder;
use liberty_code\framework\application\library\ConstApp;



// Boot application
$tabAppConfig = array(
    ConstApp::TAB_CONFIG_KEY_ROOT_DIR_PATH => $strRootAppPath
);

// Application PHP configuration
$tabAppConfigParserType = array(
    ConstParserFactory::TAB_CONFIG_KEY_TYPE => ConstStrTableParserFactory::CONFIG_TYPE_STR_TABLE_PHP,
    ConstStrTableParser::TAB_CONFIG_KEY_SOURCE_FORMAT_GET_REGEXP => '#^\<\?php\s*(.*)(\s\?\>)?\s*$#ms',
    ConstStrTableParser::TAB_CONFIG_KEY_SOURCE_FORMAT_SET_PATTERN => '<?php ' . PHP_EOL . '%1$s',
    ConstBuilder::TAB_CONFIG_KEY_CACHE_PARSER_REQUIRE => true,
    ConstBuilder::TAB_CONFIG_KEY_CACHE_FILE_PARSER_REQUIRE => true
);

$strActiveAppModeKey = null;
//$strActiveAppModeKey = 'dev';
require_once($strRootAppPath.'/vendor/liberty_code/framework/include/framework/BootHttpApp.php');



// Run application
require_once($strRootAppPath.'/vendor/liberty_code/framework/include/framework/BootAppRun.php');


