<?php

return array(
    // List modules to load
    // ******************************************************************************

    'list' => [
        // Framework modules
        // ******************************************************************************

        [
            'path' => '/vendor/liberty_code_module/view/src/view',
            'config_parser' => [
                'type' => 'string_table_php',
                'source_format_get_regexp' => '#^\<\?php\s*(.*)(\s\?\>)?\s*$#ms',
                'source_format_set_pattern' => '<?php ' . PHP_EOL . '%1$s',
                'cache_parser_require' => true,
                'cache_file_parser_require' => true
            ]
        ],
        [
            'path' => '/vendor/liberty_code_module/view/src/php_compiler',
            'config_parser' => [
                'type' => 'string_table_php',
                'source_format_get_regexp' => '#^\<\?php\s*(.*)(\s\?\>)?\s*$#ms',
                'source_format_set_pattern' => '<?php ' . PHP_EOL . '%1$s',
                'cache_parser_require' => true,
                'cache_file_parser_require' => true
            ]
        ],
        [
            'path' => '/vendor/liberty_code_module/view/src/cache',
            'config_parser' => [
                'type' => 'string_table_php',
                'source_format_get_regexp' => '#^\<\?php\s*(.*)(\s\?\>)?\s*$#ms',
                'source_format_set_pattern' => '<?php ' . PHP_EOL . '%1$s',
                'cache_parser_require' => true,
                'cache_file_parser_require' => true
            ]
        ],
        [
            'path' => '/vendor/liberty_code_module/error/src/error',
            'config_parser' => [
                'type' => 'string_table_php',
                'source_format_get_regexp' => '#^\<\?php\s*(.*)(\s\?\>)?\s*$#ms',
                'source_format_set_pattern' => '<?php ' . PHP_EOL . '%1$s',
                'cache_parser_require' => true,
                'cache_file_parser_require' => true
            ]
        ],
        [
            'path' => '/vendor/liberty_code_module/error/src/handler',
            'config_parser' => [
                'type' => 'string_table_php',
                'source_format_get_regexp' => '#^\<\?php\s*(.*)(\s\?\>)?\s*$#ms',
                'source_format_set_pattern' => '<?php ' . PHP_EOL . '%1$s',
                'cache_parser_require' => true,
                'cache_file_parser_require' => true
            ]
        ],
        [
            'path' => '/vendor/liberty_code_module/framework/src/application',
            'config_parser' => [
                'type' => 'string_table_php',
                'source_format_get_regexp' => '#^\<\?php\s*(.*)(\s\?\>)?\s*$#ms',
                'source_format_set_pattern' => '<?php ' . PHP_EOL . '%1$s',
                'cache_parser_require' => true,
                'cache_file_parser_require' => true
            ]
        ],
        [
            'path' => '/vendor/liberty_code_module/framework/src/app_mode',
            'config_parser' => [
                'type' => 'string_table_php',
                'source_format_get_regexp' => '#^\<\?php\s*(.*)(\s\?\>)?\s*$#ms',
                'source_format_set_pattern' => '<?php ' . PHP_EOL . '%1$s',
                'cache_parser_require' => true,
                'cache_file_parser_require' => true
            ]
        ],
        [
            'path' => '/vendor/liberty_code_module/framework/src/cache',
            'config_parser' => [
                'type' => 'string_table_php',
                'source_format_get_regexp' => '#^\<\?php\s*(.*)(\s\?\>)?\s*$#ms',
                'source_format_set_pattern' => '<?php ' . PHP_EOL . '%1$s',
                'cache_parser_require' => true,
                'cache_file_parser_require' => true
            ]
        ],
        [
            'path' => '/vendor/liberty_code_module/framework/src/module',
            'config_parser' => [
                'type' => 'string_table_php',
                'source_format_get_regexp' => '#^\<\?php\s*(.*)(\s\?\>)?\s*$#ms',
                'source_format_set_pattern' => '<?php ' . PHP_EOL . '%1$s',
                'cache_parser_require' => true,
                'cache_file_parser_require' => true
            ]
        ],
        [
            'path' => '/vendor/liberty_code_module/validation/src/validation',
            'config_parser' => [
                'type' => 'string_table_php',
                'source_format_get_regexp' => '#^\<\?php\s*(.*)(\s\?\>)?\s*$#ms',
                'source_format_set_pattern' => '<?php ' . PHP_EOL . '%1$s',
                'cache_parser_require' => true,
                'cache_file_parser_require' => true
            ]
        ],
        [
            'path' => '/vendor/liberty_code_module/validation/src/rule',
            'config_parser' => [
                'type' => 'string_table_php',
                'source_format_get_regexp' => '#^\<\?php\s*(.*)(\s\?\>)?\s*$#ms',
                'source_format_set_pattern' => '<?php ' . PHP_EOL . '%1$s',
                'cache_parser_require' => true,
                'cache_file_parser_require' => true
            ]
        ],
        [
            'path' => '/vendor/liberty_code_module/validation/src/cache',
            'config_parser' => [
                'type' => 'string_table_php',
                'source_format_get_regexp' => '#^\<\?php\s*(.*)(\s\?\>)?\s*$#ms',
                'source_format_set_pattern' => '<?php ' . PHP_EOL . '%1$s',
                'cache_parser_require' => true,
                'cache_file_parser_require' => true
            ]
        ],
        [
            'path' => '/vendor/liberty_code_module/datetime/src/datetime',
            'config_parser' => [
                'type' => 'string_table_php',
                'source_format_get_regexp' => '#^\<\?php\s*(.*)(\s\?\>)?\s*$#ms',
                'source_format_set_pattern' => '<?php ' . PHP_EOL . '%1$s',
                'cache_parser_require' => true,
                'cache_file_parser_require' => true
            ]
        ],
        [
            'path' => '/vendor/liberty_code_module/migration/src/migration',
            'config_parser' => [
                'type' => 'string_table_php',
                'source_format_get_regexp' => '#^\<\?php\s*(.*)(\s\?\>)?\s*$#ms',
                'source_format_set_pattern' => '<?php ' . PHP_EOL . '%1$s',
                'cache_parser_require' => true,
                'cache_file_parser_require' => true
            ]
        ],
        [
            'path' => '/vendor/liberty_code_module/handle_model/src/attribute',
            'config_parser' => [
                'type' => 'string_table_php',
                'source_format_get_regexp' => '#^\<\?php\s*(.*)(\s\?\>)?\s*$#ms',
                'source_format_set_pattern' => '<?php ' . PHP_EOL . '%1$s',
                'cache_parser_require' => true,
                'cache_file_parser_require' => true
            ]
        ],
        [
            'path' => '/vendor/liberty_code_module/handle_model/src/type',
            'config_parser' => [
                'type' => 'string_table_php',
                'source_format_get_regexp' => '#^\<\?php\s*(.*)(\s\?\>)?\s*$#ms',
                'source_format_set_pattern' => '<?php ' . PHP_EOL . '%1$s',
                'cache_parser_require' => true,
                'cache_file_parser_require' => true
            ]
        ],
        [
            'path' => '/vendor/liberty_code_module/handle_model/src/cache',
            'config_parser' => [
                'type' => 'string_table_php',
                'source_format_get_regexp' => '#^\<\?php\s*(.*)(\s\?\>)?\s*$#ms',
                'source_format_set_pattern' => '<?php ' . PHP_EOL . '%1$s',
                'cache_parser_require' => true,
                'cache_file_parser_require' => true
            ]
        ],



        // Application modules
        // ******************************************************************************

        [
            'path' => '/src/app',
            'config_parser' => [
                'type' => 'string_table_php',
                'source_format_get_regexp' => '#^\<\?php\s*(.*)(\s\?\>)?\s*$#ms',
                'source_format_set_pattern' => '<?php ' . PHP_EOL . '%1$s',
                'cache_parser_require' => true,
                'cache_file_parser_require' => true
            ]
        ]
    ]
);