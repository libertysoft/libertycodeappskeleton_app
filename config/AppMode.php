<?php

return array(
    // List application modes
    // ******************************************************************************

    'prod' => [
        'cache' => true,
        'debug' => false,
        'log.info' => true,
        'log.warning' => true,
        'log.error' => true,
        'log.exception' => true
    ],
    'dev' => [
        'cache' => false,
        'debug' => true,
        'log.info' => false,
        'log.warning' => false,
        'log.error' => false,
        'log.exception' => false
    ]
);