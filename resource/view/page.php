<?php

/** @var string $appNm */

?>
<!doctype html>
<html lang="en">
    <head>
        <title>@setDependency("title", "<?= $appNm; ?>")</title>
    </head>

    <body>
        @setDependency("body")
    </body>
</html>