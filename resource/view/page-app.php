<?php

/** @var string $appNm */

/** @var string $appVers */

?>

@extend("page")

@dependency("body")
    <div id="content">
        @setDependency("content")
    </div>

    <br />

    <footer id="footer">
        @setDependency("footer")
            @Footer: <?= $appNm; ?> - <?= $appVers; ?>
        @endSetDependency
    </footer>
@endDependency